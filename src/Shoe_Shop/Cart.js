import React, { Component } from "react";
import { connect } from "react-redux";
import {
  decreaseItemAction,
  deleteToCartAction,
  increaseItemAction,
} from "./redux/action/shoeAction";
import {
  DECREASE_ITEM,
  DELETE_ITEM,
  INCREASE_ITEM,
} from "./redux/constants/shoeConstants";

class Cart extends Component {
  renderTbody = () => {
    return this.props.cart.map((item) => {
      return (
        <tr>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>{item.price * item.soLuong}</td>
          <td>
            <img src={item.image} alt="" style={{ width: 50 }} />
          </td>
          <td>
            <button
              onClick={() => {
                this.props.handleDecrease(item);
              }}
              className="btn btn-danger"
            >
              -
            </button>
            <strong className="mx-3">{item.soLuong}</strong>
            <button
              onClick={() => {
                this.props.handleIncrease(item);
              }}
              className="btn btn-success"
            >
              +
            </button>
          </td>
          <td>
            <button
              onClick={() => {
                this.props.handleDelete(item.id);
              }}
              className="btn btn-danger"
            >
              Xóa
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div>
        <table className="table">
          <thead>
            <th>ID </th>
            <th>name</th>
            <th>Price</th>
            <th>Img</th>
            <th>Quantity</th>
            <th>Thao tác</th>
          </thead>
          <tbody>{this.renderTbody()}</tbody>
        </table>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    cart: state.shoeReducer.cart,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleDelete: (id) => {
      dispatch(deleteToCartAction(id));
    },
    handleIncrease: (id) => {
      dispatch(increaseItemAction(id));
    },
    handleDecrease: (id) => {
      dispatch(decreaseItemAction(id));
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Cart);
